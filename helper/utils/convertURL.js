import isEmpty from 'lodash/isEmpty'
import qs from 'query-string'

export const convertURLParamsToObject = (search = '') => {
  const obj = qs.parse(search)
  if (isEmpty(obj)) {
    return null
  }
  return qs.parse(search)
}
