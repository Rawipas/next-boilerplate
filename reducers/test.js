export const actionTypes = {
  SET_VALUE: 'TEST/SET_VALUE'
}

const initialState = {
  test: ''
};

const test = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_VALUE: {
      return {
        ...state,
        [action.key]: action.value
      }
    }

    default: {
      return state
    }
  }
}

export default test