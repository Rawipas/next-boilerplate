export const actionTypes = {
    RESPONSE_GET: 'RESPONSE/GET',
    RESPONSE_HANDLE: 'RESPONSE/HANDLECHANGE',
    RESPONSE_CLEAR: 'RESPONSE/CLEAR',
    MODAL_SHOW: 'MODAL/SHOW',
    MODAL_HIDE: 'MODAL/HIDE'
  }
  
  const initialState = {
    status: null,
    message: null,
    errorCode: null,
    resStatus: null,
    modal: false
  }
  
  const response = (state = initialState, action) => {
    switch (action.type) {
      case actionTypes.RESPONSE_GET: {
        return {
          ...state,
          status: action.status,
          message: action.message,
          errorCode: action.errorCode,
          resStatus: action.resStatus
        }
      }
      case actionTypes.RESPONSE_HANDLE: {
        return {
          ...state,
          [action.key]: action.value
        }
      }
      case actionTypes.RESPONSE_CLEAR: {
        return {
          ...state,
          status: null,
          message: null,
          errorCode: null,
          resStatus: null
        }
      }
      case actionTypes.MODAL_SHOW: {
        return {
          ...state,
          modal: true
        }
      }
      case actionTypes.MODAL_HIDE: {
        return {
          ...state,
          modal: false
        }
      }
      default: {
        return state
      }
    }
  }
  
  export default response
  