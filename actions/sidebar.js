
import { actionTypes as actionSidebar } from '../reducers/sidebar'

export const sidebarOn = () => {
  return async (dispatch) => {
    dispatch({
      type: actionSidebar.SIDEBAR_ON
    })

  }
}

export const sidebarOff = () => {
  return async (dispatch) => {
    dispatch({
      type: actionSidebar.SIDEBAR_OFF
    })

  }
}