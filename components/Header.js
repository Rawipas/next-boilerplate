import React, { Component } from 'react'
import { connect } from 'react-redux'
import NavLink from './NavLink'
import SideBar from './Sidebar'

class Header extends Component {
  switchSidebar = (value) => {
    this.props.switchSidebar(value)
  }

  render() {
    return (
      // <!--================Header Menu Area =================-->
      <header className="header_area">
        <div className="top_menu">
          <div className="container">
            <div className="row">
              <div className="col-4 col-lg-3">
                <div className="float-left">
                  <p>EN</p>
                </div>
              </div>
              <div className="col-8 col-lg-9">
                <div className="float-right">
                  
                  <ul className="right_side">
                    <li>
                      <NavLink activeClassName='active' href='/profile/dashboard'>
                        <a className='nav-link home-link'>My Profile</a>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink activeClassName='active' href='/cart'>
                        <a className='nav-link home-link'> 0 items</a>
                      </NavLink>
                    </li>
                    <li>
                      <a href="#">
                        Logout
                      </a>
                    </li>
                  </ul>
                 <ul className="right_side">
                      <li>
                        <NavLink activeClassName='active' href='/login'>
                          <a className='nav-link home-link'>Login</a>
                        </NavLink>
                      </li>
                    </ul>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="main_menu py-lg-5 z-head999">
          <div className="container">
            <div className='row'>
              <div className='col-6 col-md-3 col-lg-2 text-lg-center text-left'>
                <img src='/images/Logo_Test.png' className='image-logo-header' />
              </div>
              <div className='col-6 col-md-9 col-lg-0 text-right burger-nav'>
                <button className="navbar-toggler" type="button" onClick={() => this.switchSidebar(this.props.toggleSidebar)}>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                </button>
              </div>

              <div className='col-lg-9 text-center d-flex justify-content-center align-items-center'>
                <nav className="menu-side navbar-header navbar-expand-lg navbar-light w-100">
                  {/* <!-- Collect the nav links, forms, and other content for toggling --> */}
                  <div className={`navbar-collapse offset w-100 ${this.props.toggleSidebar === true && 'active'}`}>
                    <div className="row w-100 mr-0">
                      <div className="col-lg-12 pr-0">
                        <ul className="nav navbar-nav center_nav pull-right">
                          <li className="nav-item">
                            <NavLink activeClassName='active' href='/'>
                              <a className='nav-link home-link'>Home</a>
                            </NavLink>
                          </li>
                          <li className="nav-item">
                            <NavLink activeClassName='active' href='/#sector-about'>
                              <a className='nav-link home-link' href='#sector-about'>ABOUT US</a>
                            </NavLink>
                          </li>
                          <li className="nav-item">
                            <NavLink activeClassName='active' href='/#sector-recommended'>
                              <a className='nav-link home-link' href='#sector-recommended'>PRODUCTS</a>
                            </NavLink>
                          </li>
                          <li className="nav-item">
                            <NavLink activeClassName='active' href='/#sector-repurchase'>
                              <a className='nav-link home-link' href='#sector-repurchase'>PROMOTION</a>
                            </NavLink>
                          </li>
                          <li className="nav-item">
                            <NavLink activeClassName='active' href='/#sector-travel'>
                              <a className='nav-link home-link' href='#sector-travel'>TRAVEL</a>
                            </NavLink>
                          </li>
                          <li className="nav-item">
                            <NavLink activeClassName='active' href='/contact'>
                              <a className='nav-link home-link'>CONTACT</a>
                            </NavLink>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </nav>
              </div>
            </div>
          </div>
        </div>



        <SideBar toggleSidebar={this.props.toggleSidebar}/>
        

      </header>
      // <!--================Header Menu Area =================-->
    )
  }
}

const mapStateToProps = (state) => ({
  cart: state.cart
})

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)