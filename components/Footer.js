import React, { Component } from 'react'
import moment from 'moment'

export default class Footer extends Component {
    render() {
        return (
            <footer id='sticky-footer' className='text-white-50 pt-5'>
                <div className='container text-left py-3 footer-top'>
                    <div className='row'>
                        <div className='col-12 col-lg-4'>
                            <h5> BIS MARKETPLACE </h5>
                            <p className='descript-footer'>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.Since the 1500s, when an unknown printer.
                            </p>
                        </div>

                        <div className='col-12 col-lg-4'>
                            <h5> Follow Us </h5>
                            <p className='descript-footer'>
                                Since the 1500s, when an unknown printer took a galley of type and scrambled.
                            </p>
                        </div>

                        <div className='col-12 col-lg-4'>
                            <h5> Contact Us </h5>
                            <p className='descript-footer text-tag-html'>
                                {`My Company , 4578 Marmora Road, Glasgow D04 89GR 
                                Call us now: 0123-456-789 
                                Email: support@whatever.com`}
                            </p>
                        </div>
                    </div>
                </div>

                <div className='container-fluid line-white' />

                <div className='container text-left py-3'>
                    <small className='text-muted'>&copy; {moment().format('YYYY')} Ecommerce Platform</small>
                </div>
            </footer>
        )
    }
}
