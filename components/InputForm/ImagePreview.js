import React from "react";
import PropTypes from "prop-types";
import { FiMinusCircle } from 'react-icons/fi'

const ImagePreview = ({ imagefile, remove }) => {
    return imagefile.map(({ name, preview }, index) => (
        <React.Fragment key={index} >
            <div key={name} className="col-3 text-center">
                    <img src={preview} alt={name} className='img-preview' />
                    <FiMinusCircle className='btn-minus' onClick={() => remove(name)} />
                </div>
        </React.Fragment>
    ))
}

ImagePreview.propTypes = {
    imagefile: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.string))
};

export default ImagePreview;
