import React, { Component } from 'react'
import classNames from 'classnames'

export default class TextInput extends Component {
    render() {

        const { className, classLabel, meta, label, input,
            type, placeholder, name, disabled, autoComplete,
            id, maxLength, min, required } = this.props

        const labelClass = classNames(
            { [`${classLabel}`]: !meta.touched },
            { [`${classLabel}`]: !meta.error && meta.touched },
            { ' text-danger ': meta.error && meta.touched && disabled !== true })

        const inputClass = classNames(className, {
            ' is-invalid ': meta.error && meta.touched && disabled !== true
        })

        return (
            <React.Fragment>
                <div className='form-group'>
                    <label className={labelClass} htmlFor='name'>{label} {required && <span className='text-danger'>*</span>}</label>
                    <input
                        {...input}
                        autoComplete={autoComplete}
                        placeholder={placeholder}
                        className={inputClass}
                        maxLength={maxLength}
                        disabled={disabled}
                        type={type}
                        name={name}
                        min={min}
                        id={id}
                    />
                    {<p className='text-left' style={{ color: 'red', fontSize: '15px' }}>
                        {(this.props.meta.error && this.props.meta.touched && this.props.disabled !== true) && this.props.meta.error}
                    </p>}
                </div>
            </React.Fragment>
        )
    }
}
