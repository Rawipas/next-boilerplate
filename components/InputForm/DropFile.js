import React, { Component } from 'react'
import classNames from 'classnames'
import DropZone from 'react-dropzone'
import map from 'lodash/map'
import isEmpty from 'lodash/isEmpty'
import ImagePreview from './ImagePreview'
import { resizeImage, imageBlobToBase64 } from '../../helper/utils/image'


const imageMaxSize = 5242880 // bytes
const acceptedType = 'image/png,image/jpg,image/jpeg'
const acceptedTypeArray = acceptedType.split(',').map(item => {
    return item.trim()
})

export default class DropFile extends Component {



    handleChange = async (file) => {

        await map(file, async (dataFile) => {
            const imgResized = await resizeImage(dataFile)
            const imageBase64 = await imageBlobToBase64(imgResized)

            let data = {
                preview: imageBase64,
                name: dataFile.name
            }
            await this.props.input.onChange([...this.props.input.value, data])
        })


        await this.props.getFile(file)

    }

    render() {

        const {
            input,
            removeImage,
            meta: { error, touched, submitFailed }
        } = this.props

        const textClass = classNames(
            { ' text-muted ': !submitFailed && !error },
            { ' text-muted ': !submitFailed && error },
            { ' text-danger ': submitFailed && error })

            const boxClass = classNames(
                ' dropzone-container text-center',
                { ' danger ': submitFailed && error })

        return (<React.Fragment>
            <DropZone
                noClick
                accept={acceptedTypeArray}
                maxSize={imageMaxSize}
                onDrop={file => this.handleChange(file)}
            >
                {({ getRootProps, getInputProps }) => (
                    <div >
                        <div {...getRootProps()}
                            className={boxClass}>
                            <input {...getInputProps()} />
                            {isEmpty(this.props.input.value) &&
                                <React.Fragment>
                                    <p className={textClass}>Drag image to drop here...</p>
                                    <p className={textClass}> File size limit : 5 MB (for each)</p>
                                    <p className={textClass}> Only *.jpeg , *.jpg and *png images will be accepted.</p>
                                </React.Fragment>
                            }

                            {this.props.input.value &&
                                <div className='row'>
                                    <ImagePreview imagefile={this.props.input.value} remove={removeImage} />
                                </div>}
                        </div>


                    </div>

                )}

            </DropZone>


            {<p className='text-left' style={{ color: 'red', fontSize: '15px' }}>
                {(error && touched && this.props.disabled !== true) && error}
            </p>}

        </React.Fragment>)
    }
}
