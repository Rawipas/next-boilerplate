import React from 'react';
import { connect } from 'react-redux';
import { setClientState } from '../lib/redux'
import Layout from '../components/layout'

class contact extends React.Component {
  render() {
    return (
      <Layout>
        <div id='contact-page-bg'>
          <div className='container' id='contact-page'>
            <div className='row'>
              <div className='col-12 col-lg-6 layer-contact-left'>
                <div className='row'>
                  <div className='col-12 mb-2'>
                    <span className='title-sector'> STAY IN TOUCH </span>
                  </div>

                  <div className='col-12 mb-3'>
                    <span className='topic-sector'>SEND US A <br /> MESSSAGE</span>
                  </div>

                  <div className='col-12 mb-3'>
                    <p className='detail-sector'>
                      No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.
                  </p>
                  </div>

                  <div className='col-12 mb-3'>
                    <p className='detail-sector'>
                      No one rejects, dislikes, or avoids pleasure itself, because it is pleasure
                  </p>
                  </div>

                  <div className='col-12'>
                    <div className='row'>
                      <div className='col-title'>
                        <p className='detail-sector'>
                          Address
                      </p>
                      </div>
                      <div className='col-colon'>
                        <p className='detail-sector'>
                          :
                      </p>
                      </div>
                      <div className='col-detail'>
                        <p className='detail-sector'>
                          69/155 Rangsit University, Prayathai Ladkrabang, 30200
                      </p>
                      </div>
                    </div>
                  </div>

                  <div className='col-12'>
                    <div className='row'>
                      <div className='col-title'>
                        <p className='detail-sector'>
                          Mobile-no
                      </p>
                      </div>
                      <div className='col-colon'>
                        <p className='detail-sector'>
                          :
                      </p>
                      </div>
                      <div className='col-detail'>
                        <p className='detail-sector'>
                          089-029-0001, 02-100-0002
                      </p>
                      </div>
                    </div>
                  </div>
                </div>



              </div>

              <div className='col-12 col-lg-6 layer-contact-right'>
                <div className='row'>
                  <div className='col-12 mb-3' />

                  <div className='col-12 col-lg-6 mb-4 mt-lg-3 mt-5'>
                    <div className='form-group'>
                      <label className='label-input' htmlFor='fullName'>Fullname</label>
                      <input type='text' className='form-control' id='fullName' placeholder='Input' />
                    </div>
                  </div>

                  <div className='col-12 col-lg-6 mb-4 mt-lg-3'>
                    <div className='form-group'>
                      <label className='label-input' htmlFor='yourNumber'>Your Number </label>
                      <input type='text' className='form-control' id='yourNumber' placeholder='Input' />
                    </div>
                  </div>

                  <div className='col-12 mb-4'>
                    <div className='form-group'>
                      <label className='label-input' htmlFor='emailAddres'>Email Address</label>
                      <input type='email' className='form-control' id='emailAddres' placeholder='Input' />
                    </div>
                  </div>

                  <div className='col-12 mb-4'>
                    <div className='form-group'>
                      <label className='label-input' htmlFor='yourMessage'>Your Message</label>
                      <textarea className="form-control" id="yourMessage" rows="5" placeholder='Input'></textarea>
                    </div>
                  </div>
                  <div className='col-12 mb-4 text-right'>
                    <button className='btn-citrus'>SEND A MESSAGE</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

export default connect(
  (state) => state,
  { setClientState }
)(contact);