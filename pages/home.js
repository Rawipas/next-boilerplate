import React from 'react';
import { connect } from 'react-redux';
import Layout from '../components/layout'


class home extends React.Component {

  componentDidMount() {
  }

  render() {
    return (
      <Layout>
        <div id='home-page-bg'>
          <div className='container-fluid py-0 my-0 banner-top'>
            <div className='row'>
              <div className='col-12 px-0'>
                {/* IMAGE SIZE 1600x500 px */}
                <div className='box-banner'>
                  <img src='/images/home/banner2.jpg' className='home-banner' />
                </div>
              </div>
            </div>
          </div>

          <div className='container-fluid' id='home-page'>
            <div className='row' id='sector-repurchase'>
              <div className='col-12 col-lg-6 py-5 py-lg-0 layer-left'>
                <div className='row'>
                  <div className='col-12 text-left spacial-text'>
                    <span> SPECIAL PACKAGE </span>
                  </div>
                  <div className='col-12 text-left'>
                    <h2> สินค้ากลุ่มซื้อซ้ำ </h2>
                  </div>
                  <div className='col-12 text-left'>
                    <span> บริโภคสินค้าอย่างต่อเนื่อง รับผลตอบแทนมากกว่าเดิมโดยคำนึงถึง ต้นทุนทางธุรกิจ เพื่อเสนอสินค้าในราคาที่เหมาะสม พร้อมฟังก์ชั่นตัดเงิน ทุกๆต้นเดือน ไม่ต้องกังวลเรื่องจ่ายเงินอีกต่อไป </span>
                  </div>
                </div>
              </div>

              <div className='col-12 col-lg-6 px-0 layer-right'>
                <img src='/images/home/Rectangle_75.png' className='image-repurchase' />
              </div>
            </div>

            <div className='row' id='sector-register'>
              <div className='col-12 col-lg-6 px-0 layer-left order-last order-lg-first'>
                <img src='/images/home/Rectangle_76.png' className='image-register' />
              </div>

              <div className='col-12 col-lg-6 py-5 py-lg-0 layer-right order-first order-lg-last'>
                <div className='row'>
                  <div className='col-12 text-left spacial-text'>
                    <span> SPECIAL PACKAGE </span>
                  </div>
                  <div className='col-12 text-left'>
                    <h2> แพ็กเกจชุดสมัคร </h2>
                  </div>
                  <div className='col-12 text-left'>
                    <span> แนะนำให้ผู้คนรู้จักเรา นำเสนอจุดเด่นทางการตลาดและความคุ้มค่าคัดสรรสิ่งดีๆ เพื่อการบอกต่ออย่างมีประสิทธิภาพ สมัครวันนี้เพื่อรับสิทธิพิเศษจากเราพร้อมส่วนลด สินค้าที่ร่วมรายการ มากสุดถึง 25% </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

const mapStateToProps = (state) => ({
})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(home)